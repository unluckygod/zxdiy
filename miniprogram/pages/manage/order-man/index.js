// pages/order/order-list/index.js
const app = getApp();
const db = wx.cloud.database()
const product = db.collection('order_info')
const _ = db.command
const dataPageNum = 10
Page({
  data: {
    color: app.ext.color,
    orderOperate: ["全部", "待发货", "已发货", "已完成", "退款申请"],
    orderOperateIndex: 0,
    page: 1,
    orderlist: [],
    is_all: 0,
    dataNum: 0,
    state: 0,
    loadCount: 0,
    showCount: 0,
    back: 0
  },
  onLoad: function (options) {
    wx.removeStorageSync('order-type');
    // 0：代付款 1：代发货 2：待收货 3：退换货(从个人中心四个订单按钮跳转过来)
    //state= -1订单关闭（取消订单） 0代付款 1待发货 2待收货 3已完成（确认收货）退换货 
    //check 1申请退款 2通过 3未通过

    if (options.type) {
      this.setData({
        type: Number(options.type)
      })
    }
    this.setData({
      loadCount: this.data.loadCount + 1
    })
  },
  onShow: function () {
    if (this.data.back == 0) {
      this.setData({
        showCount: this.data.showCount + 1
      })
      if (this.data.showCount == this.data.loadCount) {
        this.setData({
          dataNum: 0
        })
      }
      this.changeTab();
      this.setData({
        back: 1
      })
    } else {
      this.changeTab();
      // this.getOrderList();
    }
  },
  changeTab(e) {

    let index = e ? Number(e.currentTarget.dataset.index) : 0;
    if (e) {
      this.setData({
        type: 'undefined'
      })
    }

    // this.data.state = e
    if (index == 0) {
      index = 0
    }
    if (index == 1 || this.data.type == 0) {
      this.setData({
        state: 1
      })
      index = 1;
    }
    if (index == 2 || this.data.type == 1) {
      this.setData({
        state: 2
      })
      index = 2;
    }
    if (index == 3 || this.data.type == 2) {
      this.setData({
        state: 3
      })
      index = 3;
    }
    if (index == 4 || this.data.type == 3) {
      this.setData({
        state: -10
      })
      index = 4;
    }
    if (wx.getStorageSync('order-type')) {
      const obj = wx.getStorageSync('order-type');
      this.data.state = obj.state;
      index = obj.index;
      wx.removeStorageSync('order-type');
    }
    this.setData({
      page: 1,
      orderlist: [],
      orderOperateIndex: index,
      dataNum: 0
    })
    if (index == 0) {
      this.getAllOrderList();
    } else {
      this.getOrderList();
    }
  },
  getAllOrderList() {
    let orderlist = this.data.orderlist;
    //TODO 获取订单数据
    db.collection('order_info').where({diyId:wx.getStorageSync('diyId')}).orderBy('createtime', 'desc').skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
      console.log(res.data);
      this.data.page++;
      const DATA = res.data;
      orderlist = orderlist.concat(DATA);
      this.setData({
        dataNum: this.data.dataNum + dataPageNum
      })
      if (orderlist.length != this.data.dataNum) this.data.is_all = 1;
      this.setData({
        page: this.data.page,
        is_all: this.data.is_all,
        orderlist: orderlist
      });
    })


  },
  getOrderList() {
    let orderlist = this.data.orderlist;
    if(this.data.state==-10){
      //说明是退款选项
      db.collection('order_info').orderBy('createtime', 'desc').where({
        check: _.gt(0),
        diyId:wx.getStorageSync('diyId')
      }).skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
        console.log(res.data);
        this.data.page++;
        const DATA = res.data;
        orderlist = orderlist.concat(DATA);
        this.setData({
          dataNum: this.data.dataNum + dataPageNum
        })
        if (orderlist.length != this.data.dataNum) this.data.is_all = 1;
        this.setData({
          page: this.data.page,
          is_all: this.data.is_all,
          orderlist: orderlist
        });
      })


    }else{
      db.collection('order_info').orderBy('createtime', 'desc').where({
        state: this.data.state,
        diyId:wx.getStorageSync('diyId')
      }).skip(this.data.dataNum).limit(dataPageNum).get().then(res => {
        console.log(res.data);
        this.data.page++;
        const DATA = res.data;
        orderlist = orderlist.concat(DATA);
        this.setData({
          dataNum: this.data.dataNum + dataPageNum
        })
        if (orderlist.length != this.data.dataNum) this.data.is_all = 1;
        this.setData({
          page: this.data.page,
          is_all: this.data.is_all,
          orderlist: orderlist
        });
      })


    }
    //TODO 获取订单数据
    
  },
  goDetail(e) {
    wx.setStorageSync('order-type', {
      state: this.data.state,
      index: this.data.orderOperateIndex
    });
    wx.navigateTo({
      url: `../order-info/index?order_id=${e.currentTarget.dataset.id}`
    })
  },
  operate(e) {
    const type = Number(e.currentTarget.id);
    switch (type) {
      case 0: // 确认收货
        {
          const order_id = e.detail.value.order_id;
          // wx.doodoo.fetch(`/shop/api/shop/order/order/update`, {
          //   method: 'GET',
          //   data: {
          //     id: order_id,
          //     status: 2
          //   }
          // }).then(res => {
          //   if (res && res.data.errmsg == 'ok') {
          //     this.setData({
          //       page: 1,
          //       orderlist: []
          //     })
          //     this.getOrderList();
          //   }
          // })
          break;
        };
      case 1: // 付款
        {
          const order_id = e.currentTarget.dataset.order_id;
          const trade_id = e.detail.value.trade_id;
          // wx.doodoo.fetch(`/app/pay/wxpay/index?id=${trade_id}`).then(res => {
          //   if (res && res.data.errmsg == 'ok') {
          //     let response = res.data.data;
          //     let timeStamp = response.timeStamp;
          //     //可以支付
          //     wx.requestPayment({
          //       timeStamp: timeStamp.toString(),
          //       nonceStr: response.nonceStr,
          //       package: response.package,
          //       signType: response.signType,
          //       paySign: response.paySign,
          //       success: () => {
          //         this.send_tplmsg(response.package, order_id); // 支付成功，发送支付成功模板
          //         this.setData({
          //           page: 1,
          //           orderlist: []
          //         })
          //         this.getOrderList();
          //       }
          //     });
          //   } else {
          //     wx.showModal({
          //       title: "提示",
          //       content: res.data.data,
          //       showCancel: false
          //     });
          //   }
          // });
          break;
        };
      case 2: // 取消订单
        {
          const order_id = e.detail.value.order_id;
          const that = this;
          wx.showModal({
            title: "提示",
            content: "确定取消订单吗？",
            success: function (res) {
              if (res.confirm) {
                // wx.doodoo.fetch(`/shop/api/shop/order/order/update`, {
                //   method: 'GET',
                //   data: {
                //     id: order_id,
                //     status: -1
                //   }
                // }).then(res => {
                //   if (res && res.data.errmsg == 'ok') {
                //     that.setData({
                //       page: 1,
                //       orderlist: []
                //     })
                //     that.getOrderList();
                //   }
                // })
              } else if (res.cancel) {
                console.log("用户点击取消");
              }
            }
          })
          break;
        };
      case 3: // 查看物流
        {
          wx.setStorageSync('order-type', {
            status: this.data.status,
            pay_status: this.data.pay_status,
            index: this.data.orderOperateIndex
          });
          const order = e.currentTarget.dataset.data;
          wx.navigateTo({
            url: `../logistics/index?order=${JSON.stringify(order)}`
          })
          break;
        };
    }
  },
  // 发送模板消息
  send_tplmsg: function (formId, order_id) {
    formId = formId.substring(10);
    // wx.doodoo.fetch("/api/order/sendPayTpl", {
    //   method: "GET",
    //   data: {
    //     formId: formId,
    //     orderId: order_id
    //   }
    // }, res => {
    //   console.log("发送模板消息", res);
    // });
  },
  onReachBottom: function () {
    if (!this.data.is_all) {
      this.getOrderList();
    }
  },
  onPullDownRefresh: function () {
    setTimeout(() => {
      this.setData({
        page: 1,
        orderlist: [],
        is_all: 0,
        dataNum: 0
      })
      this.getOrderList();
      wx.stopPullDownRefresh();
    }, 500)
  }
})